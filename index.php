<?php

include_once('inc/functions.php');
error_reporting(E_ERROR | E_PARSE);
$authtoken = Authenticate('user@K-Tec', 'FxZQuCP,rePS9');
$allmachines = getMachineList($authtoken);
$mach_html = "";
$allmach_ids = array();

$signal_th = array();
$signal_data = array();
//setting html for MACHINE LIST
foreach($allmachines as $onemach) {
    $allmach_ids[] = $onemach['id'];
    $mach_html.="
    <tr>
        <td data-td-name='machid'>".(isset($onemach['id'])? $onemach['id'] : '')."</td>
        <td data-td-name='machname'>".(isset($onemach['name'])? $onemach['name'] : '')."</td>
        <td data-td-name='machorgid'>".(isset($onemach['organization']['id'])? $onemach['organization']['id'] : '')."</td>
        <td data-td-name='machorgname'>".(isset($onemach['organization']['name'])? $onemach['organization']['name'] : '')."</td>
        <td data-td-name='machdevid'>".(isset($onemach['communicationUnits'][0]['id'])? $onemach['communicationUnits'][0]['id'] : '')."</td>
        <td data-td-name='machdevname'>".(isset($onemach['communicationUnits'][0]['name'])? $onemach['communicationUnits'][0]['name'] : '')."</td>
        <td data-td-name='machmodelid'>".(isset($onemach['model']['id'])? $onemach['model']['id'] : '')."</td>
        <td data-td-name='machmodelname'>".(isset($onemach['model']['name'])? $onemach['model']['name'] : '')."</td>
        <td data-td-name='machgeolat'>".(isset($onemach['geo'])? $onemach['geo']['longitude'] : '')."</td>
        <td data-td-name='machgeolon'>".(isset($onemach['geo'])? $onemach['geo']['latitude'] : '')."</td>
        <td data-td-name='machid'> </td> 
        <td data-td-name='machserial'>".(isset($onemach['serial'])? $onemach['serial'] : '')."</td>
    </tr>";
}
$signal_html = "";
foreach($allmach_ids as $k => $oneid) {
    $signals = getSignalList($authtoken,$oneid);
    
    foreach($signals as $onesig){
        if($k == 0){
            $signal_th[] = $onesig['key'];
        }
        if(array_key_exists($oneid, $signal_data)){

        }
        else {
            $signal_data[$oneid] = array();
        }
        array_push($signal_data[$oneid], array(
            'singal_key' => $onesig['key'],
            'signal_text' => $onesig['label'],
            'signal_uom' => $onesig['unit']['label'],
        ));
    }
}

//setting html for SIGNALS
foreach($signal_data as $key => $val){
    foreach ($val as $kee => $value) {
        $signal_html .= "<tr data-mach-id='".$key."'>";
        if($kee == 0){
            $signal_html .= "<td rowspan='".count($val)."'>".$key."</td>";
        }
        $signal_html .= "<td>".$value['singal_key']."</td>";
        $signal_html .= "<td>".$value['signal_text']."</td>";
        $signal_html .= "<td>".$value['signal_uom']."</td>";
        $signal_html .= "</tr>";
    }
}



?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <div class="ajax_loader">
    </div>
    <div class="container">
        <!-- ============================== navbar ============================= -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor01">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <!-- ============================== tabs ============================= -->
        <div class="tabs_container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#machlist">Machine List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#signlist">Signal List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tseries">Timeseries</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#alldata">Data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Summary</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active show" id="machlist">
                    <table id="machTable" class="table table-hover">
                        <thead>
                            <tr class="table-dark">
                                <th scope="col">MachID</th>
                                <th scope="col">MachineName</th>
                                <th scope="col">OrgId</th>
                                <th scope="col">OrgName</th>
                                <th scope="col">DeviceId</th>
                                <th scope="col">DeviceName</th>
                                <th scope="col">ModelId</th>
                                <th scope="col">ModelName</th>
                                <th scope="col">GeoLongitude [deg]</th>
                                <th scope="col">GeoLatitude [deg]</th>
                                <th scope="col">TimeZone</th>
                                <th scope="col">MachineSerialNo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $mach_html; ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="signlist">
                    <table id="signTable" class="table table-hover">
                        <thead>
                            <tr class="table-dark">
                                <th scope="col">Machine ID</th>
                                <th scope="col">Signal Key</th>
                                <th scope="col">Signal Name</th>
                                <th scope="col">Signal UoM</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php echo $signal_html; ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="tseries">
                    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>#tseries">
                    <div class="filter_container">
                        <div class="filter_item machID_dd">
                            <label for="machID_list">Machine ID</label>
                            <select id="machID_list" name="machID_list">
                                <?php 
                                foreach($allmach_ids as $singid){
                                    echo "<option ".((isset($_POST['machID_list'])&& $_POST['machID_list'] == $singid)? 'selected':  '').">".$singid."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="filter_item filter_date" id="startET">
                            <label for="startET_field">Start Date</label>
                            <input type="date" name="startET_field" id="startET_field" <?php echo isset($_POST['startET_field'])? 'value="'.$_POST['startET_field'].'"' : '';  ?> <?php echo isset($_POST['startET_field'])? 'data-value="'.(strtotime($_POST['startET_field'])).'"' : '';  ?>>
                        </div>
                        <div class="filter_item filter_date" id="endEt">
                            <label for="endEt_field">End Date</label>
                            <input type="date" name="endEt_field" id="endEt_field" <?php echo isset($_POST['endEt_field'])? 'value="'.$_POST['endEt_field'].'"' : '';  ?> <?php echo isset($_POST['endEt_field'])? 'data-value="'.(strtotime($_POST['endEt_field'])).'"' : '';  ?>>
                        </div>
                        <div class="filter_item filter_btn" id="submitbnt">
                            <input type="submit" id="get_tseries_data" name="get_tseries_data" value="Get Data / Refresh">
                        </div>
                    </div>
                    <div class="filter_container">
                        <div class="filter_item signal_list">
                            <?php 
                            $ths = "";
                                $k=1;
                                foreach($signal_th as $kee => $singID){
                                    echo "<div class='signal_cb col-md-4'><input id='".$singID."' type='checkbox' class='tseries_cb' ".(($kee < 4)? 'checked' : '' )."><label>".$singID."</label></div>";
                                    $hideme = ($k > 5)? "style='display: none'" : '';
                                    $ths .= "<th ".$hideme." data-sig-val='".$singID."'>".$singID."</th>";

                                    $k++;
                                }
                            ?>
                        </div>
                    </div>
                    </form>
                    <div class="tseries_table_container">
                        <table class="table table-hover" id="tseries_table">
                            <thead>
                                <tr class="table-dark">
                                    <th>S.No</th>
                                    <th>EpochTime</th>
                                    <?php echo $ths; ?>
                                    <th>Date</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                         $ts_tbl = "";
                                        if(isset($_POST['get_tseries_data'])){
                                            $st_tm = isset($_POST['startET_field'])? $_POST['startET_field']: '01-06-2019';
                                            $en_tm = isset($_POST['endEt_field'])? $_POST['endEt_field']: '30-06-2019';
                                            $machid = isset($_POST['machID_list'])? $_POST['machID_list']: '';
                                            $ts = getTimeSeries($authtoken, $st_tm, $en_tm, $signal_th, $machid); 
                                        }
                                        else {
                                            $startet =  '01-09-2019' ;
                                            $endet =  '30-09-2019' ;
                                            $ts = getTimeSeries($authtoken,  $startet,  $endet , $signal_th, $allmach_ids[0]);
                                        }   
                                        // print_r($ts);
                                        $tseries = array();
                                        foreach ($ts as $key => $value) {
                                            // if($key < 3) {
                                                // print_r($key);
                                                // print_r($value);
                                                // print_r($value['signal']);

                                               foreach ($value['timeseries'] as $inkey => $inval) {
                                                    //    print_r($inkey);
                                                    //    print_r($inval);
                                                       if(array_key_exists(''.$inval['time'].'', $tseries)){
                                                            $tseries[''.$inval['time'].''][$value['signal']] = array();
                                                        }
                                                        else {
                                                            $tseries[''.$inval['time'].''] = array();
                                                            $tseries[''.$inval['time'].''][$value['signal']] = array();
                                                        }
                                                        array_push($tseries[''.$inval['time'].''][$value['signal']],  $inval['value'] );
                                               }
                                            // }
                                            // else {
                                            // break;
                                            // }
                                        }

                                        // print_r($tseries);
                                        $i = 1;
                                        foreach ($tseries as $key => $value) {
                                            if($i < 5){
                                                $epochtime = $key;
                                                $ts_tbl .= "<tr><td>".$i."</td><td>".$epochtime."</td>";
                                                $j = 1; 
                                                $et = $epochtime/1000;
                                                $dt = (date("Y-m-d",$et)); // convert UNIX timestamp to PHP DateTime
                                                $tm = (date("h:i:sa",$et));

                                                // print_r($value);
                                                
                                                foreach ($signal_th as $sgthk => $sgthv) {
                                                    $hideme = ($j > 5)? "style='display: none'" : '';
                                                    if(strlen($value[$sgthv][0]) > 0){
                                                        $ts_tbl .= "<td ".$hideme ." data-sigkey='".$sgthv."' >".$value[$sgthv][0]."</td>";
                                                    }
                                                    else {
                                                        $ts_tbl .= "<td ".$hideme ." data-sigkey='".$sgthv."' >NILL</td>";
                                                    }
                                                    $j++;
                                                }
                                                $ts_tbl .= "<td>".$dt."</td><td>".$tm."</td>";
                                                
                                                $ts_tbl .= "</tr>";
                                            }
                                            $i++;
                                        }

                                        echo  $ts_tbl;
                                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="alldata">
                    <div class="filter_container">
                        <div class="filter_item machID_dd">
                            <label for="machID_list">Machine ID</label>
                            <select id="machID_list" name="machID_list">

                            <option value="109162" >109162</option><option value="109163">109163</option><option value="109165">109165</option><option value="110310">110310</option></select>
                        </div>
                        <div class="filter_item filter_date" id="startET">
                            <label for="startET_field">Start Date</label>
                            <input type="date" name="startET_field" id="startET_field">
                        </div>
                        <div class="filter_item filter_date" id="endEt">
                            <label for="endEt_field">End Date</label>
                            <input type="date" name="endEt_field" id="endEt_field">
                        </div>
                        <div class="filter_item filter_date" id="endEt">
                            <button type="button" id="get_tseries_data" name="get_tseries_data">Get Data / Refresh</button>
                        </div>
                    </div>
                    <div class="tseries_table_container">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/jquery.min.js"></script>
    <script src="assets/bootstrap.bundle.min.js"></script>
    <script src="assets/moment.js"></script>
    <script src="assets/moment-timezone-with-data.js"></script>
    <!-- <script src="main-functions.js"></script> -->
    <script src="domfunction.js"></script>

</body>

</html>