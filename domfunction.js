(function ($) {
    jQuery(document).ready(function () {
 
        $(window).bind('hashchange', function () {
            $('ul.nav-tabs a[href^="' + document.location.hash + '"]').click();
        });

        if (document.location.hash.length) {
            $(window).trigger('hashchange');
        }
        
        jQuery('.ajax_loader').css({
            opacity: "0",
            visibility: "hidden"
        }, 200)
      

        // TSERIES CB WORK
        jQuery('input.tseries_cb').change(function(){
            var thisid = jQuery(this).attr('id');
            if(jQuery(this).is(':checked')){
                jQuery('table#tseries_table tr th[data-sig-val="'+thisid+'"]').css('display', 'table-cell');
                jQuery('table#tseries_table tr td[data-sigkey="'+thisid+'"]').css('display', 'table-cell');
            }
            else{
                jQuery('table#tseries_table tr th[data-sig-val="'+thisid+'"]').css('display', 'none');
                jQuery('table#tseries_table tr td[data-sigkey="'+thisid+'"]').css('display', 'none');

            }


        })

    }) // doc ready
})(jQuery);