<?php 
/**
 *  @param $user = user@K-Tec
 *  @param $pass = FxZQuCP,rePS9
 */
function Authenticate($user, $pass) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.proemion.com/authz/oauth2/token",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_POSTFIELDS => "username=$user&password=$pass&grant_type=password&client_id=portal",
    CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    $jsonres = json_decode($response, true);
    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } 
    else {
        return $jsonres['accessToken'];
    };

}

function getMachineList($authtoken){

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.proemion.com/machines?limit=1000&offset=0&geo=1",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    // CURLOPT_POSTFIELDS => "limit=1000&offset=0&geo=1",
    CURLOPT_HTTPHEADER => array(
        "content-type: application/json",
        "Authorization: Bearer $authtoken"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    $jsonres = json_decode($response, true);
    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } 
    else {
        return $jsonres;
    }
}

function getSignalList($authtoken, $machId) {
    $siglist = array();
    
    // foreach($machId as $key => $mach_id){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.proemion.com/machines/$machId/signals",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/json",
            "Authorization: Bearer $authtoken"
        ),
        ));
    
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
            $siglist = $err;
        } 
        else {
            $siglist = json_decode($response, true);
        }
    // }
    return $siglist;
}

function getTimeSeries($authtoken, $startEt, $endEt, $signals, $machID) {
    $curl = curl_init();
    $myurl;
    $myDetails;
    $myRes = array();
    $newStart;
    $newEnd;
    
    $sigObj = array();
    $sigKeys = array();

    // foreach ($signals as $key => $value) {
    //     print_r($signals);
    //     // die();
    //     $vvt = json_decode($value, true);
    //     foreach ($vvt as $k2 => $v2) {
    //         $sigKeys[] = $v2['key'];
    //     }
           
    // }
    
    foreach ($signals as $k3 => $v3) {
        $sigObj[$k3] = array(
            "signal" => $v3,
            "aggregationFunction" => "raw",
            "groupBy" => array (
                "type" => "machine",
                "id" =>  $machID
            )
        );
    }

    $newStart = strtotime($startEt) * 1000; 
    $newEnd = strtotime($endEt) * 1000;
    // echo $newStart;
    // $newStart = 1567296000000; 
    // $newEnd = 1569801600000;

    $myDetails = array(
        "from" => $newStart,
        "to" => $newEnd,
        "limit" => 100000,
        "bucketSize" => 3600000,
        "queries" => $sigObj
    );
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.proemion.com/timeseries",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 120,
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_POST => true,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($myDetails),
    CURLOPT_HTTPHEADER => array(
        "content-type: application/json",
        "Authorization: Bearer $authtoken"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    
    if ($err) {
        $myRes= $err;
    } 
    else {
        $myRes = json_decode($response, true);
    }
    return $myRes;
}


?>